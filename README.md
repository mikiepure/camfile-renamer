# Camera File Renamer

撮影した画像・映像のファイル名を撮影日時・撮影機種に基づいて変更する。

ファイル名は `<撮影日時> [<撮影機種>, <元ファイル名>].<拡張子>` に変換される。具体例としては `2020-01-01 18-48-40 [iPhone SE, DSC_0001].jpg` のようになる。詳細は以下表の通り。

| 項目         | 生成方法                                                                                                                                                                |
| ------------ | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| 撮影日時     | メタ情報が存在する場合はメタ情報のものを使用する。存在しない場合、ファイルの作成日時と更新日時のうち古いほうを使用する。                                                |
| 撮影機種     | メタ情報が存在する場合はメタ情報のものを使用する。存在しない場合は空になる。ツールバーのテキストボックス (Model) に任意の撮影機種を指定すると、これを優先して使用する。 |
| 元ファイル名 | 拡張子を除いたオリジナルのファイルの名前。本ツールで変換したファイル名の場合、変換前のファイル名になる。                                                                |
| 拡張子       | オリジナルのファイルの拡張子。必ず小文字になる。                                                                                                                        |

## Screenshots

![スクリーンショット](Screenshot.png)

## Requirements

### for use

- Windows 10
- .NET Framework 4.7.2
- [MediaInfo 32 bit DLL v20.09](https://mediaarea.net/ja/MediaInfo/Download/Windows)

### for development

- Visual Studio Community 2019

## How to use

1. `MediaInfo.dll` (32 bit) を `CameraFileRenamer.exe` と同じフォルダに格納する
2. `CameraFileRenamer.exe` を実行してウィンドウを表示する
3. ファイル名を変更したい画像・映像ファイルをウィンドウのリストに Drag & Drop する
4. ツールバーの「名前の変更を適用」ボタンを押下する

## How to build

1. `MediaInfo.dll` (32 bit) をフォルダ直下に配置する
2. Visual Studio でプロジェクトを開く
3. ビルドを実行する

## Special thanks

- 動画のメタデータ抽出に [MediaInfo](https://mediaarea.net/ja/MediaInfo) を使用しています。
- ツールバーのアイコンに [Silk icon set 1.3](http://www.famfamfam.com/lab/icons/silk/) を使用しています。
