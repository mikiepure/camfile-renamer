﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace CameraFileRenamer
{
    internal class CameraFile
    {
        private enum MediaType
        {
            IMAGE, VIDEO
        }

        private static readonly string[] SUPPORT_IMAGE_EXT_LIST = new string[]{ ".jpg", ".jpeg" };
        private static readonly string[] SUPPORT_VIDEO_EXT_LIST = new string[]{ ".mov", ".mp4" };
        private static readonly Regex RENAME_PATTERN = new Regex(@"^\d{4}-\d{2}-\d{2} \d{2}-\d{2}-\d{2} \[.+, (?<orgname>.+?)\]\..+$");
        private static readonly string RENAME_PATTERN_DATETIME = "yyyy-MM-dd HH-mm-ss";

        private CameraFile(string path, string model, MediaType type)
        {
            FilePath = path;
            FileName = Path.GetFileName(path);
            Model = model;

            var matches = RENAME_PATTERN.Match(FileName);
            if (matches.Success)
            {
                OrgFileName = matches.Groups["orgname"].Value;
            }
            else
            {
                OrgFileName = Path.GetFileNameWithoutExtension(path);
            }

            switch (type)
            {
                case MediaType.IMAGE:
                    {
                        var mediaTime = MediaInfoAPI.GetImageCreationDate(FilePath);
                        if (mediaTime != null)
                        {
                            MetaCreationDate = mediaTime.Value.ToString(RENAME_PATTERN_DATETIME);
                        }
                        else
                        {
                            MetaCreationDate = GetFileDateTime().ToString(RENAME_PATTERN_DATETIME);
                        }
                        MetaModel = MediaInfoAPI.GetImageCreationModel(FilePath);
                    }
                    break;

                case MediaType.VIDEO:
                    {
                        var mediaTime = MediaInfoAPI.GetVideoCreationDate(FilePath);
                        if (mediaTime != null)
                        {
                            MetaCreationDate = mediaTime.Value.ToString("yyyy-MM-dd HH-mm-ss");
                        }
                        else
                        {
                            MetaCreationDate = GetFileDateTime().ToString("yyyy-MM-dd HH-mm-ss");
                        }
                        MetaModel = MediaInfoAPI.GetVideoCreationModel(FilePath);
                    }
                    break;
            }
        }

        public static CameraFile Create(string path, string model)
        {
            var extension = Path.GetExtension(path);

            if (SUPPORT_IMAGE_EXT_LIST.Any(ext => string.Compare(ext, extension, true) == 0))
            {
                return new CameraFile(path, model, MediaType.IMAGE);
            }

            if (SUPPORT_VIDEO_EXT_LIST.Any(ext => string.Compare(ext, extension, true) == 0))
            {
                return new CameraFile(path, model, MediaType.VIDEO);
            }

            return null;
        }

        public ListViewItem ToListViewItem()
        {
            var newFileName = MakeNewFileName();
            var item = new ListViewItem(new string[] { FileName, newFileName })
            {
                Tag = this
            };
            if (FileName == newFileName)
            {
                item.ForeColor = Color.Gray;
            }
            return item;
        }

        public bool ApplyMove()
        {
            var newFilePath = Path.Combine(Path.GetDirectoryName(FilePath), MakeNewFileName());
            if (FilePath != newFilePath)
            {
                File.Move(FilePath, newFilePath);
                return true;
            }

            return false;
        }

        public string FilePath { get; private set; }

        public string Model { get; set; }

        private string FileName { get; set; }

        private string OrgFileName { get; set; }

        private string MetaCreationDate { get; set; }

        private string MetaModel { get; set; }

        private string MakeNewFileName()
        {
            var ext = Path.GetExtension(FilePath).ToLower();
            var model = (Model == "") ? (MetaModel) : (Model);
            return $"{MetaCreationDate} [{model}, {OrgFileName}]{ext}";
        }

        private DateTime GetFileDateTime()
        {
            var creationTime = File.GetCreationTime(FilePath);
            var lastWriteTime = File.GetLastWriteTime(FilePath);
            if (creationTime < lastWriteTime)
            {
                return creationTime;
            }
            else
            {
                return lastWriteTime;
            }
        }
    }
}
