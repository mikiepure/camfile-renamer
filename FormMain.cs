﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CameraFileRenamer
{
    public partial class FormMain : Form
    {
        public FormMain()
        {
            InitializeComponent();
        }

        private void ToolStripButtonNew_Click(object sender, EventArgs e)
        {
            // Remove all items
            listViewFilename.Items.Clear();
        }

        private void ToolStripButtonRemoveSelectedItems_Click(object sender, EventArgs e)
        {
            // Remove selected items
            foreach (var item in listViewFilename.SelectedItems)
                listViewFilename.Items.Remove((ListViewItem)item);
        }

        private void ListViewFilename_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                e.Effect = DragDropEffects.Copy;
            }
            else
            {
                e.Effect = DragDropEffects.None;
            }
        }

        private void ListViewFilename_DragDrop(object sender, DragEventArgs e)
        {
            AddPathsToListView((string[])e.Data.GetData(DataFormats.FileDrop, false));
        }

        private async void AddPathsToListView(string[] paths)
        {
            var dialog = new ProgressDialog("Loading files...", paths.Length);

            using (var tokenSource = new CancellationTokenSource())
            {
                var cancelToken = tokenSource.Token;

                // Process to each path by worker thread
                Task task = Task.Run(() =>
                {
                    int i = 0;
                    foreach (var path in paths)
                    {
                        // Cancel the loop if cancel button is pressed
                        if (cancelToken.IsCancellationRequested)
                            break;

                        // Add path to list view by main thread
                        Invoke((MethodInvoker)delegate
                        {
                            if (File.Exists(path))
                            {
                                var camfile = CameraFile.Create(path, toolStripTextBoxModel.Text);
                                if (camfile != null)
                                {
                                    var existSamePath = listViewFilename.Items.Cast<ListViewItem>().Any(item => ((CameraFile)item.Tag).FilePath == path);
                                    if (!existSamePath)
                                    {
                                        listViewFilename.Items.Add(camfile.ToListViewItem());
                                    }
                                }
                            }
                        });

                        // Update progress on the dialog
                        dialog.Progress.Report(i);
                        i++;
                    }

                    // Close progress dialog by main thread
                    Invoke((MethodInvoker)delegate { dialog.Close(); });
                }, tokenSource.Token);

                dialog.ShowDialog(this);

                tokenSource.Cancel();

                await task;
            }
        }

        private void ToolStripButtonRename_Click(object sender, EventArgs e)
        {
            int movedNum = 0;

            foreach (var item in listViewFilename.Items)
            {
                if (((CameraFile)((ListViewItem)item).Tag).ApplyMove())
                {
                    movedNum++;
                }
            }

            MessageBox.Show($"{movedNum} 個のファイルの名前を変更しました");
            listViewFilename.Items.Clear();
        }

        private void ListViewFilename_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Delete)
            {
                // Remove selected items
                foreach (var item in listViewFilename.SelectedItems)
                    listViewFilename.Items.Remove((ListViewItem)item);
            }
        }

        private void ToolStripTextBoxModel_TextChanged(object sender, EventArgs e)
        {
            var model = ((ToolStripTextBox)sender).Text;

            // make new camera files for the list view
            var camfiles = new List<CameraFile>();
            foreach (var item in listViewFilename.Items)
            {
                var camfile = (CameraFile)((ListViewItem)item).Tag;
                camfile.Model = model;
                camfiles.Add(camfile);
            }

            // apply new camera files to the list view
            listViewFilename.Items.Clear();
            listViewFilename.Items.AddRange(camfiles.Select(camfile => camfile.ToListViewItem()).ToArray());
        }

    }
}
