﻿namespace CameraFileRenamer
{
    partial class FormMain
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージド リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.listViewFilename = new System.Windows.Forms.ListView();
            this.columnHeaderBefore = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderAfter = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.toolStrip = new System.Windows.Forms.ToolStrip();
            this.toolStripButtonNew = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonRemoveSelectedItems = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonRename = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabelModel = new System.Windows.Forms.ToolStripLabel();
            this.toolStripTextBoxModel = new System.Windows.Forms.ToolStripTextBox();
            this.toolStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // listViewFilename
            // 
            this.listViewFilename.AllowDrop = true;
            this.listViewFilename.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeaderBefore,
            this.columnHeaderAfter});
            this.listViewFilename.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listViewFilename.FullRowSelect = true;
            this.listViewFilename.GridLines = true;
            this.listViewFilename.HideSelection = false;
            this.listViewFilename.Location = new System.Drawing.Point(0, 25);
            this.listViewFilename.Name = "listViewFilename";
            this.listViewFilename.Size = new System.Drawing.Size(624, 416);
            this.listViewFilename.TabIndex = 0;
            this.listViewFilename.UseCompatibleStateImageBehavior = false;
            this.listViewFilename.View = System.Windows.Forms.View.Details;
            this.listViewFilename.DragDrop += new System.Windows.Forms.DragEventHandler(this.ListViewFilename_DragDrop);
            this.listViewFilename.DragEnter += new System.Windows.Forms.DragEventHandler(this.ListViewFilename_DragEnter);
            this.listViewFilename.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ListViewFilename_KeyDown);
            // 
            // columnHeaderBefore
            // 
            this.columnHeaderBefore.Text = "変更前のファイル名";
            this.columnHeaderBefore.Width = 300;
            // 
            // columnHeaderAfter
            // 
            this.columnHeaderAfter.Text = "変更後のファイル名";
            this.columnHeaderAfter.Width = 300;
            // 
            // toolStrip
            // 
            this.toolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButtonNew,
            this.toolStripSeparator1,
            this.toolStripButtonRemoveSelectedItems,
            this.toolStripSeparator2,
            this.toolStripButtonRename,
            this.toolStripSeparator3,
            this.toolStripLabelModel,
            this.toolStripTextBoxModel});
            this.toolStrip.Location = new System.Drawing.Point(0, 0);
            this.toolStrip.Name = "toolStrip";
            this.toolStrip.Size = new System.Drawing.Size(624, 25);
            this.toolStrip.TabIndex = 1;
            // 
            // toolStripButtonNew
            // 
            this.toolStripButtonNew.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonNew.Image = global::CameraFileRenamer.Properties.Resources.table;
            this.toolStripButtonNew.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonNew.Name = "toolStripButtonNew";
            this.toolStripButtonNew.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonNew.Text = "新規リスト作成";
            this.toolStripButtonNew.ToolTipText = "新規リスト作成";
            this.toolStripButtonNew.Click += new System.EventHandler(this.ToolStripButtonNew_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripButtonRemoveSelectedItems
            // 
            this.toolStripButtonRemoveSelectedItems.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonRemoveSelectedItems.Image = global::CameraFileRenamer.Properties.Resources.table_delete;
            this.toolStripButtonRemoveSelectedItems.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonRemoveSelectedItems.Name = "toolStripButtonRemoveSelectedItems";
            this.toolStripButtonRemoveSelectedItems.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonRemoveSelectedItems.Text = "選択項目を削除";
            this.toolStripButtonRemoveSelectedItems.ToolTipText = "選択項目を削除";
            this.toolStripButtonRemoveSelectedItems.Click += new System.EventHandler(this.ToolStripButtonRemoveSelectedItems_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripButtonRename
            // 
            this.toolStripButtonRename.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonRename.Image = global::CameraFileRenamer.Properties.Resources.table_edit;
            this.toolStripButtonRename.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonRename.Name = "toolStripButtonRename";
            this.toolStripButtonRename.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonRename.Text = "名前の変更を適用";
            this.toolStripButtonRename.Click += new System.EventHandler(this.ToolStripButtonRename_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripLabelModel
            // 
            this.toolStripLabelModel.Name = "toolStripLabelModel";
            this.toolStripLabelModel.Size = new System.Drawing.Size(44, 22);
            this.toolStripLabelModel.Text = "Model:";
            // 
            // toolStripTextBoxModel
            // 
            this.toolStripTextBoxModel.Font = new System.Drawing.Font("Yu Gothic UI", 9F);
            this.toolStripTextBoxModel.Name = "toolStripTextBoxModel";
            this.toolStripTextBoxModel.Size = new System.Drawing.Size(100, 25);
            this.toolStripTextBoxModel.TextChanged += new System.EventHandler(this.ToolStripTextBoxModel_TextChanged);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(624, 441);
            this.Controls.Add(this.listViewFilename);
            this.Controls.Add(this.toolStrip);
            this.Name = "FormMain";
            this.Text = "Camera Filename Maker";
            this.toolStrip.ResumeLayout(false);
            this.toolStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView listViewFilename;
        private System.Windows.Forms.ColumnHeader columnHeaderBefore;
        private System.Windows.Forms.ColumnHeader columnHeaderAfter;
        private System.Windows.Forms.ToolStrip toolStrip;
        private System.Windows.Forms.ToolStripButton toolStripButtonRename;
        private System.Windows.Forms.ToolStripButton toolStripButtonRemoveSelectedItems;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripLabel toolStripLabelModel;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBoxModel;
        private System.Windows.Forms.ToolStripButton toolStripButtonNew;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
    }
}

