﻿using System;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using MediaInfoLib;

namespace CameraFileRenamer
{
    /// <summary>
    /// MediaInfo.dll (MediaInfoDLL.cs) の Wrapper クラスとして、本プログラムから参照・使用するために便利な API を提供する
    /// </summary>
    internal static class MediaInfoAPI
    {
        /// <summary>
        /// 画像ファイルの作成日時を取得する
        /// </summary>
        /// <param name="path">情報を取得したいファイルのパス</param>
        /// <returns>作成日時 (取得失敗時は null を返す)</returns>
        public static DateTime? GetImageCreationDate(string path)
        {
            var dateTimeOriginal = GetImageInfo(path, 0x9003);  // ID=0x9003: DateTimeOriginal
            if (dateTimeOriginal != "")
            {
                return DateTime.ParseExact(dateTimeOriginal, "yyyy:MM:dd HH:mm:ss", null);
            }

            return null;
        }

        /// <summary>
        /// 画像ファイルを作成した機器のモデル名を取得する
        /// </summary>
        /// <param name="path">情報を取得したいファイルのパス</param>
        /// <returns>機器のモデル名 (取得失敗時は空文字列を返す)</returns>
        public static string GetImageCreationModel(string path)
        {
            return GetImageInfo(path, 0x0110);  // ID=0x0110: Model
        }

        /// <summary>
        /// 画像ファイルの情報を取得する
        /// </summary>
        /// <param name="path">情報を取得したいファイルのパス</param>
        /// <param name="id">取得対象とする情報の EXIF ID</param>
        /// <returns>指定した EXIF ID の情報 (取得失敗時は空文字列)</returns>
        private static string GetImageInfo(string path, int id)
        {
            try
            {
                using (var bitmap = new Bitmap(path))
                {
                    foreach (var item in bitmap.PropertyItems)
                    {
                        if (item.Id == id && item.Type == 2)    // Type=2: Ascii
                        {
                            var model = Encoding.ASCII.GetString(item.Value).Trim(new char[] { '\0' });
                            return model;
                        }
                    }
                }
            }
            catch (ArgumentException e)
            {
                MessageBox.Show($"画像ファイル '{path}' からの情報 (ID: ${id}) の取得に失敗しました: {e}", "エラー", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            return "";
        }

        /// <summary>
        /// 映像ファイルの作成日時を取得する
        /// </summary>
        /// <param name="path">情報を取得したいファイルのパス</param>
        /// <returns>作成日時 (取得失敗時は null を返す)</returns>
        public static DateTime? GetVideoCreationDate(string path)
        {
            var creationDate = GetVideoInfo(path, "com.apple.quicktime.creationdate");
            if (creationDate != "")
            {
                return DateTime.Parse(creationDate);
            }

            return null;
        }

        /// <summary>
        /// 映像ファイルを作成した機器のモデル名を取得する
        /// </summary>
        /// <param name="path">情報を取得したいファイルのパス</param>
        /// <returns>機器のモデル名 (取得失敗時は空文字列を返す)</returns>
        public static string GetVideoCreationModel(string path)
        {
            return GetVideoInfo(path, "com.apple.quicktime.model");
        }

        /// <summary>
        /// 映像ファイルの情報を取得する
        /// </summary>
        /// <param name="path">情報を取得したいファイルのパス</param>
        /// <param name="param">取得対象とする情報のパラメータ</param>
        /// <returns>指定したパラメータの情報 (取得失敗時は空文字列)</returns>
        private static string GetVideoInfo(string path, string param)
        {
            var mediaInfo = new MediaInfo();
            if (mediaInfo.Open(path) == 1)  /* 1: File opened */
            {
                var model = mediaInfo.Get(StreamKind.General, 0, param);
                mediaInfo.Close();
                return model;
            }

            return "";
        }
    }
}
