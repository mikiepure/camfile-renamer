﻿using System;
using System.Windows.Forms;

namespace CameraFileRenamer
{
    public partial class ProgressDialog : Form
    {
        /// <summary>
        /// Create and initialize dialog.
        /// </summary>
        /// <param name="message">Message on the dialog</param>
        /// <param name="maximum">Maximum value of the progress</param>
        public ProgressDialog(string message, int maximum)
        {
            InitializeComponent();

            Progress = new Progress<int>(value =>
            {
                // Update UI by progress value
                progressBar.Value = value;
                labelProgress.Text = $"{progressBar.Value} / {progressBar.Maximum}";

                // Refresh UI components
                Refresh();
            });

            labelMessage.Text = message;
            progressBar.Maximum = maximum;

            Progress.Report(0);
        }

        public IProgress<int> Progress
        {
            get;
        }
    }
}
